package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;



public class ColorGrid implements IColorGrid{
  private int numberOfRows;
  private int numberOfCols;
  private List<List<Color>> grid;

  public ColorGrid(int rows, int cols) {
    this.numberOfRows = rows;
    this.numberOfCols = cols;
    this.grid = new ArrayList<>();
    for (int i=0; i < rows; i++) {
      grid.add(new ArrayList<>());
    }
    for (int row=0; row < rows; row++) {
      for (int col=0; col < cols; col++) {
        grid.get(row).add(col, null);
      }
    }
  }

  @Override
  public int rows() {
    return numberOfRows;
  }

  @Override
  public int cols() {
    return numberOfCols;
  }

  @Override
  public List<CellColor> getCells() {
    List<CellColor> cellColorList = new ArrayList<>();
    for (int row=0; row < numberOfRows; row++) {
      for (int col=0; col < numberOfCols; col++) {
        CellPosition cp = new CellPosition(row, col);
        cellColorList.add(new CellColor(cp, get(cp)));
      }
    }
    return cellColorList; 
  }

  @Override
  public Color get(CellPosition pos) {
    int r = pos.row();
    int c = pos.col();
    Color color = grid.get(r).get(c);
    return color;
  }

  @Override
  public void set(CellPosition pos, Color color) {
    int r = pos.row();
    int c = pos.col();
    
    if (r > numberOfRows || c > numberOfCols) {
      throw new IndexOutOfBoundsException();
    }
    else {
      grid.get(r).set(c, color);
    }
  }   
}

