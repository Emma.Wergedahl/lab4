package no.uib.inf101.gridview;

import javax.swing.JPanel;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.IColorGrid;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.List;

public class GridView extends JPanel {
  IColorGrid param;
  private static final double OUTERMARGIN = 30;
  private static final Color MARGINCOLOR = Color.LIGHT_GRAY;

  public GridView(IColorGrid parameter) {
    this.setPreferredSize(new Dimension(400, 300));
    this.param = parameter;
  }

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    drawGrid(g2);
  }

  public void drawGrid(Graphics2D ob1) {
    Rectangle2D rectangleBack = new Rectangle2D.Double(OUTERMARGIN, OUTERMARGIN, (getWidth() - 2*OUTERMARGIN), (getHeight() - 2*OUTERMARGIN));
    ob1.setColor(MARGINCOLOR);
    ob1.fill(rectangleBack);
    CellPositionToPixelConverter x = new CellPositionToPixelConverter(rectangleBack, param, OUTERMARGIN);
    drawCells(ob1, this.param, x);
  }

  private static void drawCells(Graphics2D ob2, CellColorCollection ob3, CellPositionToPixelConverter ob4) {
    List<CellColor> colorList = ob3.getCells();
    for (CellColor colorCell: colorList) {
      Rectangle2D cellRectangle = ob4.getBoundsForCell(colorCell.cellPosition());
      if (colorCell.color() == null) {
        ob2.setColor(Color.DARK_GRAY);
      } else {
        ob2.setColor(colorCell.color());
      }
      ob2.fill(cellRectangle);
    }

  }

}
